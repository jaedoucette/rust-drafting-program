# rust-drafting-program

This project implements a drafting library very nearly from scratch, based internally on just changing individual pixel colors in OpenGL via glium, and exposing an interface with graphical primitives more akin to the Java-like Processing package.

The main purpose of the project is pedogogical: it is an opportunity to practice and further develop my knowledge of the Rust programming language by designing a larger project.

The project may eventually be useful to others, and I may eventually use it as a teaching tool in my classes, because it should expose an easy-to-use interface for making simple drawings in Rust.