extern crate glium;

use glium::glutin;
use glium::Surface;


/* This module should encapsulate all the OpenGL nastyness
 *  into a simple interface for drawing individual pixels on
 *  a frame buffer. 
 *  
 *  Example usage:
 *    let gs = GraphicsSurface::new();
 *    
 *    gs.add_pixel(0,0); //black pixel in upper left.
 *
 *    //Refresh display until user presses close button.
 *    while !gs.time_to_close_window() {
 *      gs.refresh(); 
 *    }
 */

struct Pixel { x: u32, y: u32}

pub struct GraphicsSurface {
    events_loop: glutin::EventsLoop,
    window: glium::Display,
    height: u32,
    width: u32,
    pipeline: glium::Program,
    pixel_list : Vec<Pixel>,
}

#[derive(Copy, Clone)]
struct Vertex {
    position: [f32; 2],
}

implement_vertex!(Vertex, position);


impl GraphicsSurface {
    pub fn new() -> GraphicsSurface {
        let events = glutin::EventsLoop::new();
        let w = GraphicsSurface::initialize_window(&events);
        let code = GraphicsSurface::assemble_pipeline(&w);
        GraphicsSurface {
            events_loop: events,
            window: w,
            height: 200,
            width: 200,
            pipeline: code,
            pixel_list: Vec::new(),
        }
    }

    fn initialize_window(events_loop: &glutin::EventsLoop) -> glium::Display {
        let window = glutin::WindowBuilder::new()
            .with_dimensions(glium::glutin::dpi::LogicalSize::new(200.0,200.0))
            .with_title("John's Rust Graphics Program");
        let context = glutin::ContextBuilder::new();
        glium::Display::new(window, context, events_loop).unwrap()
    }

    fn assemble_pipeline(window: &glium::Display) -> glium::Program {
        let vertex_shader_src = r#"
        #version 140
        in vec2 position;
        void main() {
            gl_Position = vec4(position, 0.0, 1.0);
        }"#;

        let fragment_shader_src = r#"
        #version 140
        out vec4 color;
        void main(){
            color = vec4(0.0,0.0,0.0,1.0);
        }"#;

        glium::Program::from_source(window, vertex_shader_src, fragment_shader_src, None).unwrap()
    }

    pub fn time_to_close_window(&mut self) -> bool {
        let mut result = false;
        {
            let close_window_callback = |event: glutin::Event| match event {
                glutin::Event::WindowEvent { event, .. } => match event {
                    glutin::WindowEvent::CloseRequested => result = true,
                    _ => (),
                },
                _ => (),
            };
            self.events_loop.poll_events(close_window_callback);
        }
        result
    }

    fn put_pixels(&self, buffer : &mut glium::Frame) -> () {

        let mut vertex_listing = vec![];
        for p in &self.pixel_list{
            let x_offset = p.x as i32 - self.width as i32;
            let y_offset = p.y as i32 - self.height as i32;
            let x_location = x_offset as f32 / self.width as f32;
            let y_location = -1.0*y_offset as f32 / self.height as f32; //flip to mimic raster screen.
            let vertex = Vertex {
                position: [x_location, y_location],
            };
            vertex_listing.push(vertex);
        }

        let vertex_buffer = glium::VertexBuffer::new(&(self.window), &vertex_listing).unwrap();
        let indices = glium::index::NoIndices(glium::index::PrimitiveType::Points);

        buffer.draw(&vertex_buffer, &indices, &(self.pipeline), &glium::uniforms::EmptyUniforms, &Default::default()).unwrap();
    }

    fn put_pixel(&self, x: u32, y: u32, buffer : &mut glium::Frame) -> () {
        let x_offset = x as i32 - self.width as i32;
        let y_offset = y as i32 - self.height as i32;
        let x_location = x_offset as f32 / self.width as f32;
        let y_location = -1.0*y_offset as f32 / self.height as f32; //flip to mimic raster screen.

        let vertex1 = Vertex {
            position: [x_location, y_location],
        };
        let square = vec![vertex1];

        let vertex_buffer = glium::VertexBuffer::new(&(self.window), &square).unwrap();
        let indices = glium::index::NoIndices(glium::index::PrimitiveType::Points);

        buffer.draw(&vertex_buffer, &indices, &(self.pipeline), &glium::uniforms::EmptyUniforms, &Default::default()).unwrap();
    }

    pub fn refresh(&self){
        let mut buffer = self.window.draw();
        buffer.clear_color(1.0, 1.0, 1.0, 1.0);
        
        //for p in &self.pixel_list {
        //    self.put_pixel(p.x, p.y, &mut buffer);
        //}
        self.put_pixels(&mut buffer);

        buffer.finish().unwrap();
    }


    pub fn add_pixel(&mut self, x : u32, y : u32){
        self.pixel_list.push(Pixel{ x: x, y: y});
    }
}
