
#[macro_use]
extern crate glium;
use glium::Surface;
pub mod graphics_surface;
use graphics_surface::GraphicsSurface;

fn main() {
    let mut gs = GraphicsSurface::new();

    let mut counter = 0;
    while !gs.time_to_close_window() {
        gs.refresh();
        gs.add_pixel(10+counter,10);
        gs.add_pixel(5+counter,20);
        gs.add_pixel(0+counter,30);
        counter = counter+1;
        std::thread::sleep(std::time::Duration::new(0,1000/24));
    }
}
